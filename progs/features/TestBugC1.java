class Main {
    public static void main(String[] args){
		System.out.println(new TestBugC1().Start());
	}
} 
    
class TestBugC1 { //Comparison of booleans.
	public int Start() {
	  int result;
    boolean count;
    result = 0;
    count = true;
    while (count <= false) {
        result = result + count;
        count = count + 1;
    }
    return result;

    }

}