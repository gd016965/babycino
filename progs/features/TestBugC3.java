class Main {
    public static void main(String[] args){
		System.out.println(new TestBugC3().Start());
	}
}

class TestBugC3 {
	public int Start() {
		int x;
		int y;
		int z;
		x = 7;
		y = 7;
		z = 6;
	
		if (z <= x && x <= y) {
            
			System.out.println(1);
            
		} else {
            
			System.out.println(0);
		}
		
		return 0; //returns 1 if the compiler works
	}
}