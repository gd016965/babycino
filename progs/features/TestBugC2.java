class Main {
    public static void main(String[] args){
		System.out.println(new TestBugC2().Start());
	}
}
        
class TestBugC2 { // Check if false is returned if x equals y
	public int Start() {
		int x;
		int y;
		x = 7;
		y = 7;
	
		if (x <= y) {
            
			System.out.println(1);
            
		} else {
            
			System.out.println(0);
            
		}
		
		return 0; //return 1 if the compiler works
	}
}